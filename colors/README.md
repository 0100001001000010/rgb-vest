# colors

This folder contains the definitions of color schemes. A list of color schemes to send to the client is autmoatically generated from the contents of this directory. It is read by main.py at start.

## Search process

All files in this directory ending in `.json` will be treated as color schemes. The contents of subfolders will not be checked. If you wish to disable a color scheme without deleting it, change the file name to something that does not end in `.json`. For example, to disable `example.json`, you may change its name to `example.json.disabled`.

## Creating or editing a color scheme

The portion of the file name before the `.json` is the ID that will be used to refer to the color scheme internally. It must contain only letters, numbers, and `_`, and must not start with a number. It must be unique, but that will be enforced by the filesystem. It is recommended to use camelCase for the names here, and the name should be reasonably descriptive of the color scheme.

Each file contains two properties, `"name"` and `"colors"`. `"name"` represents a human readable name that will appear in the web UI. It should be short but descriptive of the color scheme. `"colors"` is an array where each element represents a color. Each color is also an array containing 3 elements where the first represents the red component, the second contains the green component, and the third contains the blue component. You are allowed to use the same color more than once. If you do so, it will appear additional times in a sequence, and it will appear more often when randomized.

All effects that cycle through colors in order will determine that order based on the order they are listed in the file, so make sure you choose an order that looks good.

You only need to provide colors that you intend to be displayed for an extended period of time, targeted at the end of a fade operation, or otherwise actually intended. You do not need to provide "transition" colors. If an effect requires "transition" colors (such as a fade effect), it will generate them itself.