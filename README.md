# Hardware setup

1, Connect the first strip's data input to pin D18 on the raspberry pi. This is the sixth pin from the corner, on the side of the GPIO header closest to the edge.
    * If you are powering your LEDs with 5V, you are supposed to use a level shifter because the pi uses 3.3V levels. However, if you don't have one, you can probably get away with not using one. It might not work (though it did for me), but it shouldn't break anything.
    * If you are powering your LEDs directly from a lithium ion battery or any other source less than 4.7V (which 3.3V is 70% of), you should not need a level shifter.
2. Power the LEDs and the raspberry pi.
    * If using the same power supply for both, make sure that both get power from the supply directly. Do not power the LEDs through the pins on the Raspberry Pi. You will probably get away with using the 5V pin for one strip, and might even get away with using the 3.3V pin for one strip or the 5V pin for two strips if you're careful and lucky. But you risk crashes due to voltage drops and damage to the pi due to excessive current, so it's a bad idea.
    * If using a different power supply, the grounds must be connected to each other, and the LEDs must be powered before the pi. If the pi is powered first, the LEDs might attempt to draw power through the data pin, which will probably damage both the LEDs and the pi.

# Software Setup

1. Make a fresh ISO of the minimal Rapberry Pi OS image. You may be able to get away with using an old one, but a fresh one is more likely to work
2. On the boot partition, create a file named `ssh` and leave it blank
3. On the boot partiton, create a file named `wpa_supplicant.conf` and fill it with the following to connect to your phone's hotspot:
    ```
    country=YOUR_COUNTRY_CODE
    ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
    update_config=1

    network={
        ssid="YOUR_HOTSPOT_NAME"
        psk="YOUR_HOTSPOT_PASSWORD"
    }
    ```
    Don't worry, we only downloading a little bit of data now, and then probably never downloading anything again.
4. Take a look at the colorSchemes directory to see if you want to define or modify any color schemes, the effects directory to see if you want to define or modify any effects, and loader.py to see if there are any settings you want to change.
5. Copy the contents of the repo into `/home/pi/rgb/` in the rootfs partition of the SD card. If there is not enough room, boot the pi, shut it down, and try again. (It will increase the data partition up to the size of your SD card.)
6. Connect your workstation to the hotspot, power up the pi, wait for it to connect, and ssh into it.
    * The command to ssh in is `ssh <username>@<IP address>`. The username is `pi` by default. You should be able to find the IP address in your phone's hotspot settings, but you also may (or may not) be able to use `raspberrypi` in place of it.
    * The default password is `raspberry`.
    * You will be advised to change the default password and given instructions on how to do so, and it is recommended to follow that advice.
7. Run the following commands to install the dependencies:
    ```
    sudo apt-get update
    sudo apt-get install python3-pip
    sudo pip install adafruit-circuitpython-neopixel
    ```
7. Run `sudo crontab -e`, pick your favorite editor, and add a line that says `@reboot python3 /home/pi/rgb/main.py`.
8. Run `sudo reboot` to reboot the pi. It should be running the software after the reboot.
    * The LEDs start out being turned off, so the only indication that the boot has completed is the pi showing up in the list of devices connected to your hotspot.

# Use

## Power on
Boot the pi. It should eventually connect to your hotspot. When this happens, open `<raspberrypi's IP address>:8180` in your device's browser, and have fun!

## Power off
There are two ways to do this:
1. Select the "Power down" effect from the effects menu and check the box indicating that you are sure you want to.
2. ssh into the pi and run `sudo systemctl poweroff`.

Once the green LED on the pi flashes 10 times, you can safely power off. If you are using separate power supplies, disconnect the pi first.