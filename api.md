# API overview

An API request is a request to a URL in the form of `<host>:<port>/api?<queryString>`. A request to read something will just contain a single key in the query string, and a request to change something will be of the form `<key>=<value>`. Here is what is accessible with the API:

# colorSchemes (read only)
The color schemes available, returned as JSON. More details in `colors/README.md`.

# colorScheme
The ID of the currently active color scheme.

# effects (read only)
The effects available, returned as JSON. Only includes ID, name, and description. More details in `effects/README.md`.

# effect
The ID of the currently active effect.

# options (read only)
The options available, returned as JSON. More details in `effects/README.md`.

# option-<optionId>
The current value of an option.