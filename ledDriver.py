import os
import sys
from loader import colorSchemes, effects, SRC_ROOT, STRING_LENGTH, REGION_MAP, FREQUENCY
if(os.uname().machine in ('x86', 'x86_64')):
    sys.path.insert(0, SRC_ROOT)
    import neopixelEmulator as neopixel
    PIN = 0
else:
    import neopixel
    import board
    PIN = board.D18
import time
import json

# The color order of the strip.
COLOR_ORDER = neopixel.RGB

# How often we update the options, every X LED updates
UPDATE_OPTIONS_FREQ = 10


# Initialization
colorSchemeId = sys.argv[1]
effectId = sys.argv[2]
effect = effects[effectId](
    STRING_LENGTH, colorSchemes[colorSchemeId]['colors'], REGION_MAP, FREQUENCY)
pixels = neopixel.NeoPixel(PIN, STRING_LENGTH, bpp=len(
    COLOR_ORDER), pixel_order=COLOR_ORDER)

# LED update loop
while True:
    try:
        with open('/tmp/rgb-options.json') as file:
            effect.options = json.JSONDecoder().decode(file.read())
    except:
        pass

    for _ in range(UPDATE_OPTIONS_FREQ):
        nextColors = effect.genNextColors()
        pixels[0:STRING_LENGTH] = nextColors
        time.sleep(1 / FREQUENCY)
