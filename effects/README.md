# effects

This folder contains the definitions of color change effects. A list of color schemes to send to the client is autmoatically generated from the contents of this directory. It is read by main.py at start.

## Search process

All files in this directory ending in `.py` will be treated as effects. The contents of subfolders will not be checked. If you wish to disable an effect without deleting it, change the file name to something that does not end in `.py`. For example, to disable `example.py`, you may change its name to `example.py.disabled`.

## Creating or editing a color effect

The portion of the file name before the `.py` is the ID that will be used to refer to the effect internally. It must contain only letters, numbers, and `_`, and must not start with a number. It must be unique, but that will be enforced by the filesystem. It is recommended to use camelCase for the names here, and the name should be reasonably descriptive of the color scheme.

Each file contains a class called `Effect`, which contains all of the information about the effect. It contains at least the following things:

* `name`: A human readable name that will appear in the UI. This should be short and descriptive.
* `description`: A human readable description that will appear in the UI if the user requests it. This should be longer and more descriptive than the name
* `__init__(colorScheme, regionMap, frequency, colorOrder)`: Will be called to initialize the color effect. It will be given the following properties:
    * `stringLength`: The length of the string of LEDs
    * `colorScheme`: A list of colors to be used, each of which is a tuple of 3 elements because that is what the adafruit neopixel library uses. For the best user experience, all colors here should be used. No additional colors should be used, except to interpolate between colors provided.
    * `regionMap`: An object mapping LEDs on the string into regions. More details in the top of `../loader.py`.
    * `frequency`: A number representing how many times per second the string will be refreshed. This is useful for effects which are timing dependant.
* `options`: An object containing options to display in the UI, described in more detail below.
* `genNextColors(options)`: Generate the next colors, in the form of a list of tuples of 3 values for the Adafruit neopixel library, to display on the string. Takes option values from the UI.

`name`, `description`, and `__init__()` must be defined immediately as they are needed in the process of selecting and loading a color scheme, but the rest can be generated at runtime as part of `__init__()`.

## Options

Some effects may require options to be displayed on the UI. For example, the user may want to configure the rate of a flash or fade effect, or manually re-randomize a random mix. The effect can define which options are available through the `options` property. This is a dictionary where the keys represent internal identifiers, and the values are dictionaries with the following properties:

* `name`: A human-readable name to appear in the UI
* `type`: One of `frequency`, `color`, `bool`, `button`, or `list`.
    * `frequency` allows the user to set a value in seconds or minutes per beat, beats per second, beats per minute, or with tapping at the desired frequency.
    * `color` allows the user to set a color in RGB (automatically converted into appropriate color order) using hex codes.
    * `bool` allows the user to choose something that should be true or false using a checkbox.
    * `button` allows the user to press a button, and the value gets set to true for just one color generation before getting reset to false.
    * `list` allows the user to choose from a list.
* `choices`: If `type==list`, this is a list of all possible choices.
* `value`: The current value, which is configurable at run time by the user. The value defined in the source code is the default value.

When calling `genNextColors()`, all that is needed is the current value, so a much simpler format is used. It is simply a dictionary using id:value pairs.