from ast import literal_eval
from datetime import datetime, timedelta


class Effect:
    name = "Ultra high visibility"
    description = "Rapid white flash, separate effect to enable quick switching"

    def __init__(self, stringLength, colorScheme, regionMap, frequency):
        self.options = {}
        self.stringLength = stringLength
        # We do not need to keep track of:
        #  - colorScheme, because it isn't used anywhere
        #  - regionMap, because it isn't used anywhere
        #  - frequency, because it isn't used anywhere

        # Initialize state
        self._lastSwitchTime = datetime.now()
        self._curState = False

    def genNextColors(self):
        currentTime = datetime.now()
        if currentTime > self._lastSwitchTime + timedelta(seconds=0.1):
            self._curState = not self._curState
            self._lastSwitchTime = currentTime
        ledColor = (255, 255, 255) if self._curState else (0, 0, 0)
        return [ledColor] * self.stringLength
