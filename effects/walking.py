from datetime import datetime, timedelta

FRONT_COLOR = (255, 255, 255)
BACK_COLOR = (255, 0, 0)
SIGNAL_COLOR = (255, 64, 0)
TIME_DELTA = 0.3


class Effect:
    name = "Walking"
    description = "Make walking at night slightly safer"

    def __init__(self, stringLength, colorScheme, regionMap, frequency):
        self.options = {
            'frontAndBack': {
                'name': 'Front/back lights',
                'type': 'bool',
                'value': 'true'
            },
            'left': {
                'name': 'Left signal',
                'type': 'bool',
                'value': 'false'
            },
            'right': {
                'name': 'Right signal',
                'type': 'bool',
                'value': 'false'
            }
        }
        self.stringLength = stringLength
        self.regionMap = regionMap
        # We do not need to keep track of:
        #  - colorScheme, because it isn't used anywhere
        #  - frequency, because it isn't used anywhere

        # Initialize state
        self._lastSwitchTime = datetime.now()   # Time when state last changed
        # Whether the left/right lights should be on if enabled
        self._on = False

    def _generateRegionColors(self, size, foreground, background):
        if foreground == None and background == None:
            result = [(0, 0, 0)] * size
        elif background == None:
            result = [foreground] * size
        elif foreground == None:
            result = [background] * size
        else:
            result = [background] * size
            lower = int(size / 4)
            upper = 3 * lower
            result[lower:upper] = [foreground] * (upper-lower)

        return result

    def _applyRegionColors(self, ledColors, regionId, foreground, background):
        lower, upper = self.regionMap[regionId]
        ledColors[lower:upper] = self._generateRegionColors(
            upper - lower, foreground, background)
        return ledColors

    def genNextColors(self):
        currentTime = datetime.now()

        # If we are due to change the state, do so
        if currentTime > self._lastSwitchTime + timedelta(seconds=TIME_DELTA):
            self._lastSwitchTime = currentTime

            if self._on:
                self._on = False
            else:
                self._on = True

        ledColors = [(0, 0, 0)] * self.stringLength
        ledColors = self._applyRegionColors(
            ledColors,
            'frontLeft',
            SIGNAL_COLOR if self.options['left']['value'] == 'true' and self._on else None,
            FRONT_COLOR if self.options['frontAndBack']['value'] == 'true' else None
        )
        ledColors = self._applyRegionColors(
            ledColors,
            'backLeft',
            SIGNAL_COLOR if self.options['left']['value'] == 'true' and self._on else None,
            BACK_COLOR if self.options['frontAndBack']['value'] == 'true' else None
        )
        ledColors = self._applyRegionColors(
            ledColors,
            'frontRight',
            SIGNAL_COLOR if self.options['right']['value'] == 'true' and self._on else None,
            FRONT_COLOR if self.options['frontAndBack']['value'] == 'true' else None
        )
        ledColors = self._applyRegionColors(
            ledColors,
            'backRight',
            SIGNAL_COLOR if self.options['right']['value'] == 'true' and self._on else None,
            BACK_COLOR if self.options['frontAndBack']['value'] == 'true' else None
        )

        return ledColors
