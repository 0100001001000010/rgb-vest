from ast import literal_eval
from datetime import datetime, timedelta
import random


class Effect:
    name = "Flash"
    description = "Flash colors in a color scheme"

    def __init__(self, stringLength, colorScheme, regionMap, frequency):
        self.options = {
            'brightness': {
                'name': 'Brightness',
                'type': 'num',
                'min': '0',
                'max': '100',
                'value': '100'
            },
            'frequency': {
                'name': 'Frequency',
                'type': 'frequency',
                'value': '3'
            },
            'colorDomain': {
                'name': 'Random color domain',
                'type': 'list',
                'choices': ['Full strip', 'Regions', 'Individual LED'],
                'value': 'Full strip'
            },
            'turnOff': {
                'name': 'Turn off between flashes',
                'type': 'bool',
                'value': 'true'
            },
            'random': {
                'name': 'Randomize colors',
                'type': 'bool',
                'value': 'true'
            },
            'doSecondary': {
                'name': 'Do secondary flash',
                'type': 'bool',
                'value': False
            },
            'secondaryFreq': {
                'name': 'Secondary flash frequency',
                'type': 'frequency',
                'value': '0.75'
            }
        }
        self.stringLength = stringLength
        self.colorScheme = colorScheme
        self.regionMap = regionMap
        # We do not need to keep track of:
        #  - frequency, because it isn't used anywhere

        # Initialize state
        self._lastSwitchTime = datetime.now()   # Time when state last changed
        # The last time a flash was replaced with a secondary flash
        self._lastSecondaryTime = datetime.now()
        # Index of current color, if using sequential mode
        self._curColor = 0
        self._on = False                        # Whether the lights are currently on
        self._ledColors = [(0, 0, 0)] * self.stringLength

    def genNextColors(self):
        currentTime = datetime.now()

        # If we are due to change the state, do so
        # If we are turning the lights off between flashes, half the period/double the frequency to make it look like it's measuring color changes, not state changes
        if self.options['turnOff']['value'] == 'true':
            lightsOffMultiplier = 0.5
        else:
            lightsOffMultiplier = 1
        if currentTime > self._lastSwitchTime + timedelta(microseconds=1 / float(self.options['frequency']['value']) * 1000000 * lightsOffMultiplier):
            self._lastSwitchTime = currentTime

            # If the light is on and it should turn off, turn it off. If the light is off or shouldn't turn off, turn it on if needed and advance the colors
            if self._on and self.options['turnOff']['value'] == 'true':
                self._on = False
                self._ledColors = [(0, 0, 0)] * self.stringLength
            else:
                self._on = True

                def secondaryFlashDue():
                    return self.options['doSecondary']['value'] == 'true' and \
                        self.currentTime > self._lastSecondaryTime + timedelta(microseconds=1 / float(
                            self.options['secondaryFreq']['value']) * 1000000 * lightsOffMultiplier)

                if secondaryFlashDue():
                    self._ledColors = [(255, 255, 255)] * self.stringLength
                    self._lastSecondaryTime = datetime.now()
                elif self.options['random']['value'] == 'true':
                    colorDomain = self.options['colorDomain']['value']
                    if colorDomain == 'Full strip':
                        ledColor = random.choice(self.colorScheme)
                        self._ledColors = [ledColor] * self.stringLength
                    elif colorDomain == 'Regions':
                        self._ledColors = [(0, 0, 0)] * self.stringLength
                        for regionKey in self.regionMap.keys():
                            first, last = self.regionMap[regionKey]
                            ledColor = random.choice(self.colorScheme)
                            self._ledColors[first:last] = [
                                ledColor] * (last - first)
                    else:
                        self._ledColors = []
                        for _ in range(self.stringLength):
                            self._ledColors.append(
                                random.choice(self.colorScheme))
                else:
                    self._curColor = (
                        self._curColor + 1) % len(self.colorScheme)
                    ledColor = self.colorScheme[self._curColor]
                    self._ledColors = [ledColor] * self.stringLength

        brightnessMultiplier = int(self.options['brightness']['value']) / 100

        return [tuple([int(channel * brightnessMultiplier) for channel in color]) for color in self._ledColors]
