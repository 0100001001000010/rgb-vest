import os


class Effect:
    name = "Power down"
    description = "Turn the LEDs off and cleanly shut down the pi"

    def __init__(self, stringLength, colorScheme, regionMap, frequency):
        self.options = {
            "confirm": {
                "name": "Are you sure?",
                "type": "bool",
                "value": "false"
            }
        }
        self.stringLength = stringLength
        # We do not need to keep track of:
        #  - colorScheme, because it isn't used anywhere
        #  - regionMap, because it isn't used anywhere
        #  - frequency, because it isn't used anywhere

    def genNextColors(self):
        if self.options['confirm']['value'] == 'true':
            os.system('systemctl poweroff')
        return [(0, 0, 0)] * self.stringLength
