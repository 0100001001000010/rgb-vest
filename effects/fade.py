from ast import literal_eval
from datetime import datetime, timedelta
import random


class Effect:
    name = "Fade"
    description = "Flash colors in a color scheme"

    def __init__(self, stringLength, colorScheme, regionMap, frequency):
        self.options = {
            'brightness': {
                'name': 'Brightness',
                'type': 'num',
                'min': '0',
                'max': '100',
                'value': '100'
            },
            'frequency': {
                'name': 'Frequency',
                'type': 'frequency',
                'value': '3'
            },
            'colorDomain': {
                'name': 'Random color domain',
                'type': 'list',
                'choices': ['Full strip', 'Regions', 'Individual LED'],
                'value': 'Full strip'
            },
            'turnOff': {
                'name': 'Turn off between flashes',
                'type': 'bool',
                'value': 'true'
            },
            'random': {
                'name': 'Randomize colors',
                'type': 'bool',
                'value': 'true'
            },
            'doSecondary': {
                'name': 'Do secondary flash',
                'type': 'bool',
                'value': False
            },
            'secondaryFreq': {
                'name': 'Secondary flash frequency',
                'type': 'frequency',
                'value': '0.75'
            },
            'transitionPercent': {
                'name': '% Transition time',
                'type': 'num',
                'min': '0',
                'max': '100',
                'value': '25'
            }
        }
        self.stringLength = stringLength
        self.colorScheme = colorScheme
        self.regionMap = regionMap
        self.frequency = frequency

        # Initialize state
        self._lastSwitchTime = datetime.now()   # Time when state last changed
        # The last time a flash was replaced with a secondary flash
        self._lastSecondaryTime = datetime.now()
        # Index of current color, if using sequential mode
        self._curColor = 0
        self._on = False                        # Whether the lights are currently on

        self._ledColors = [(0, 0, 0)] * self.stringLength
        self._oldColors = [(0, 0, 0)] * self.stringLength
        self._newColors = [(0, 0, 0)] * self.stringLength

    def genNextState(self, then):
        """
        Generate a future LED state. Assumes that the future LED state being generated is to be used immediately after the last one, as this function depends on global state.

        Parameters:
            self (Effect): The instance of the class
            then (datetime.datetime): The point in time when this state is intended to be displayed. Note that this function assumes that the caller has already validated that a new state is due at this time: this function will not do it. However, this function will check whether a secondary flash is due at that time.
        Returns:
            List: A list of length self.stringLength containing tuples representing the colors
        """

        # If we are turning the lights off between flashes, half the period/double the frequency to make it look like it's measuring color changes, not state changes
        if self.options['turnOff']['value'] == 'true':
            lightsOffMultiplier = 0.5
        else:
            lightsOffMultiplier = 1

        # If the light is on and it should turn off, turn it off. If the light is off or shouldn't turn off, turn it on if needed and advance the colors
        if self._on and self.options['turnOff']['value'] == 'true':
            self._on = False
            ledColors = [(0, 0, 0)] * self.stringLength
        else:
            self._on = True

            def secondaryFlashDue():
                return self.options['doSecondary']['value'] == 'true' and \
                    then > self._lastSecondaryTime + timedelta(microseconds=1 / float(
                        self.options['secondaryFreq']['value']) * 1000000 * lightsOffMultiplier)

            if secondaryFlashDue():
                ledColors = [(255, 255, 255)] * self.stringLength
                self._lastSecondaryTime = datetime.now()
            elif self.options['random']['value'] == 'true':
                colorDomain = self.options['colorDomain']['value']
                if colorDomain == 'Full strip':
                    ledColor = random.choice(self.colorScheme)
                    ledColors = [ledColor] * self.stringLength
                elif colorDomain == 'Regions':
                    ledColors = [(0, 0, 0)] * self.stringLength
                    for regionKey in self.regionMap.keys():
                        first, last = self.regionMap[regionKey]
                        ledColor = random.choice(self.colorScheme)
                        ledColors[first:last] = [
                            ledColor] * (last - first)
                else:
                    ledColors = []
                    for _ in range(self.stringLength):
                        ledColors.append(
                            random.choice(self.colorScheme))
            else:
                self._curColor = (
                    self._curColor + 1) % len(self.colorScheme)
                ledColor = self.colorScheme[self._curColor]
                ledColors = [ledColor] * self.stringLength

        brightnessMultiplier = int(self.options['brightness']['value']) / 100

        return [tuple([int(channel * brightnessMultiplier) for channel in color]) for color in self._ledColors]

    def genNextColors(self):
        def stateStale():
            return currentTime > self._lastSwitchTime + timedelta(microseconds=1 / float(self.options['frequency']['value']) * 1000000 * lightsOffMultiplier)

        def nextStateTime():
            return datetime.now() + timedelta(microseconds=1 / float(self.options['frequency']['value']) * 1000000 * lightsOffMultiplier)

        if stateStale():
            self._oldColors = self._newColors
            self._newColors = genNextState(nextStateTime())
            self._lastSwitchTime = datetime.now()
        else:
            def interpolate(old, new):
                def interpolateChannel(old, new):
                    timePassed = datetime.now() - self._lastSwitchTime
                    timeAllocated = 1 / \
                        float(self.options['frequency']['value']
                              ) * 1000000 * lightsOffMultiplier * (int(options['transitionPercent']['value']) / 100)
                    progressMultiplier = min(
                        timePassed / timeAllocated,
                        1
                    )
                    return old + (new - old) * progressMultiplier

                if len(old) != len(new):
                    raise ValueError('old and new must be the same length')

                return tuple([interpolateChannel(old[i], new[i]) for i in range(len(old))])

            for i in range(self.stringLength):
                self._curColor[i] = interpolate(
                    self._oldColor[i], self._newColor[i])
