class Effect:
    name = "Off"
    description = "Turn the LEDs off without shutting down the pi"

    def __init__(self, stringLength, colorScheme, regionMap, frequency):
        self.options = {}
        self.stringLength = stringLength
        # We do not need to keep track of:
        #  - colorScheme, because it isn't used anywhere
        #  - regionMap, because it isn't used anywhere
        #  - frequency, because it isn't used anywhere

    def genNextColors(self):
        return [(0, 0, 0)] * self.stringLength
