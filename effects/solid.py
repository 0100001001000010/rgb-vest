from ast import literal_eval


class Effect:
    name = "Solid color"
    description = "Display a solid color, either from the color scheme or a custom one."

    def __init__(self, stringLength, colorScheme, regionMap, frequency):
        self.options = {
            'brightness': {
                'name': 'Brightness',
                'type': 'num',
                'min': '0',
                'max': '100',
                'value': '100'
            },
            'color': {
                'name': 'Color',
                'type': 'list',
                'choices': [str(color) for color in colorScheme] + ['Custom'],
                'value': str(colorScheme[0])
            },
            'custom': {
                'name': 'Custom color (choose Custom above)',
                'type': 'color',
                'value': '000000'
            }
        }
        self.stringLength = stringLength
        # We do not need to keep track of:
        #  - colorScheme, because it is already stored in self.options and isn't needed anywhere else.
        #  - regionMap, because it isn't used anywhere
        #  - frequency, because it isn't used anywhere

    def _hexToTuple(self, hexColor):
        # Remove the # so it doesn't matter whether it's there
        stripped = hexColor.lstrip('#')
        return (
            int(stripped[0:2], 16),
            int(stripped[2:4], 16),
            int(stripped[4:6], 16)
        )

    def genNextColors(self):
        if (self.options['color']['value'] == 'Custom'):
            ledColor = ([self._hexToTuple(self.options['custom']['value'])]
                        * self.stringLength)
        else:
            if (self.options['color']['value'] not in self.options['color']['choices']):
                # literal_eval should prevent problems from malicious input, but just in case crash the program if receiving an input we shouldn't have.
                raise ValueError('POTENTIAL ATTACK DETECTED!')
            ledColor = literal_eval(self.options['color']['value'])

        brightnessMultiplier = int(self.options['brightness']['value']) / 100

        ledColor = tuple([int(channel * brightnessMultiplier)
                          for channel in ledColor])
        return [ledColor] * self.stringLength
