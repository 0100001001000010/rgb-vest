# This emulates a neopixel strip for testing on a desktop/laptop. It creates a server for a webpage that shows the
# current state of the LEDs. It aims for API compatibility with the Adafruit neopixel library, so it implements a lot
# of unnecessary hardware-related stuff. Note that the white channel, if present, is ignored because I have no clue how
# to convert that to plain RGB for displays.


import subprocess
from collections import UserList
from PIL import Image
import os
import socket
import time


# Does literally nothing because we aren't using hardware, but provided for API compatibility
GRB = 'GRB'
GRBW = 'GRBW'
RGB = 'RGB'
RGBW = 'RGBW'


class NeoPixel(UserList):

    # Initialize what we actually need for basic emulation
    def __init__(self, pin, n, *, bpp=3, brightness=1.0, auto_write=True, pixel_order=None):
        self.n = n
        self._autoWrite = auto_write
        self.data = [(0,) * bpp] * n
        self._imv = subprocess.Popen(
            '/usr/bin/imv', stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        self._socket = socket.socket(family=socket.AF_UNIX)
        time.sleep(1)  # Bad workaround to avoid a race condition
        self._socket.connect(os.path.join(
            os.environ['XDG_RUNTIME_DIR'], f'imv-{self._imv.pid}.sock'))
        self._shown = self.data
        if (self._autoWrite):
            self.show()

    def write(self):
        self.show()

    def show(self):
        self._shown = self.data
        image = Image.new('RGB', (30 * self.n, 30))
        for led in range(self.n):
            for x in range(led * 30, led * 30 + 30):
                for y in range(30):
                    image.putpixel((x, y), self._shown[led])
        image.save('/tmp/neopixel.png')
        self._socket.send(b'open /tmp/neopixel.png\n')
        self._socket.send(b'close 1\n')

    # Do nothing because we haven't done anything to hardware that we need to redo

    def deinit(self):
        pass

    def fill(self, color):
        self.data = [color] * n

    def __repr__(self):
        return "[" + ", ".join([str(x) for x in self]) + "]"

    def __setitem__(self, index, newValue):
        self.data[index] = newValue
        if(self._autoWrite):
            self.show()
