import json
import importlib
import os
import sys

# Configuration TODO: Move all configuration here
STRING_LENGTH = 50      # How many LEDs are on the connected string
# Map which LEDs are installed over which area. Each key represents an area, and each value is a tuple representing the
# upper and lower bounds of the area. Valid areas are:
#  - 'frontLeft', 'backLeft', 'frontRight', 'backRight': self explanatory
#  - 'extra': any extra LEDs on the end that there isn't a place for, and which may or may not still be used by effects
# The boundaries are zero indexed. The lower bound is inclusive, and the upper bound is exclusive.
REGION_MAP = {
    'frontLeft': (0, 13),
    'backLeft': (13, 25),
    'backRight': (25, 37),
    'frontRight': (37, 50),
}
# How many times we attempt to refresh the LEDs every second. Increase if transitions which should be smooth aren't smooth, or timing isn't precise enough. Decrease if the CPU is pinned
FREQUENCY = 100


# Load color schemes
SRC_ROOT = os.path.dirname(__file__)
os.chdir(os.path.join(SRC_ROOT, 'colors'))
allFiles = os.listdir()
colorFiles = list(filter(lambda file: file.endswith('.json'), allFiles))
colorFiles.sort()
colorSchemes = {}
for file in colorFiles:
    fileHandler = open(file)
    colorScheme = json.JSONDecoder().decode(fileHandler.read())
    fileHandler.close()

    colorScheme['colors'] = [tuple(color) for color in colorScheme['colors']]
    colorSchemes[file.split('.')[0]] = colorScheme


# Load effects
os.chdir(os.path.join(SRC_ROOT, 'effects'))
sys.path.insert(0, os.path.join(SRC_ROOT, 'effects'))
allFiles = os.listdir()
effectFiles = list(filter(lambda file: file.endswith('.py'), allFiles))
effectFiles.sort()
effects = {}
for file in effectFiles:
    effects[file.split('.')[0]] = importlib.import_module(
        file.split('.')[0]).Effect
