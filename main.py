from urllib.parse import unquote
import mimetypes
from loader import colorSchemes, effects, SRC_ROOT, STRING_LENGTH, REGION_MAP, FREQUENCY
from wsgiref.simple_server import make_server
import os
import sys
import time
import json
import subprocess


# Which port the server runs on
PORT = 8180


def setGlobals(newColorSchemeId, newEffectId):
    global colorSchemeId
    global effectId
    global colorScheme
    global effect

    colorSchemeId = newColorSchemeId
    colorScheme = colorSchemes[colorSchemeId]
    effectId = newEffectId
    effect = effects[effectId](
        STRING_LENGTH, colorScheme['colors'], REGION_MAP, FREQUENCY)
    return


setGlobals(list(colorSchemes.keys())[0], list(effects.keys())[0])


def startLedProcess():
    with open('/tmp/rgb-options.json', 'w') as file:
        file.write(json.JSONEncoder().encode(effect.options))

    global ledProcess
    ledProcess = subprocess.Popen(
        [
            '/usr/bin/python3',
            f'{SRC_ROOT}/ledDriver.py',
            colorSchemeId,
            effectId
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )


def killLedProcess():
    ledProcess.kill()


startLedProcess()


def serverApp(environ, start_response):
    reqPath = environ['PATH_INFO']

    # Return true if the given option can be set to the given value on the given effect

    def validateOption(effect, option, value):
        # TODO: Make this actually do something useful
        return True

    # If requesting /api, run the API on the query string
    if reqPath == '/api':
        querySplit = environ['QUERY_STRING'].split('=')
        key = querySplit[0]
        value = unquote(querySplit[1]) if len(querySplit) >= 2 else ''

        if key == 'colorSchemes':
            start_response('200 OK', [('Content-Type', 'text/json')])
            return [bytes(json.JSONEncoder().encode(colorSchemes).encode())]

        elif key == 'colorScheme':
            if value == '':
                start_response('200 OK', [('Content-Type', 'text/plain')])
                return [bytes(colorSchemeId.encode())]
            elif value in colorSchemes.keys():
                setGlobals(value, effectId)
                killLedProcess()
                startLedProcess()
                start_response('204 No Content', [
                               ('Content-Type', 'text/plain')])
                return [b'204 No Content']
            else:
                start_response('400 Bad Request', [
                               ('Content-Type', 'text/plain')])
                return [b'400 Bad Request']

        elif key == 'effects':
            start_response('200 OK', [('Content-Type', 'text/json')])
            response = {}
            for key in effects.keys():
                effectToAdd = effects[key]
                response[key] = {
                    'name': effectToAdd.name,
                    'description': effectToAdd.description
                }
            return [bytes(json.JSONEncoder().encode(response).encode())]

        elif key == 'effect':
            if value == '':
                start_response('200 OK', [('Content-Type', 'text/plain')])
                return [bytes(effectId.encode())]
            elif value in effects.keys():
                setGlobals(colorSchemeId, value)
                killLedProcess()
                startLedProcess()
                start_response('204 No Content', [
                               ('Content-Type', 'text/plain')])
                return [b'204 No Content']
            else:
                start_response('400 Bad Request', [
                               ('Content-Type', 'text/plain')])
                return [b'400 Bad Request']

        elif key == 'options':
            start_response('200 OK', [('Content-Type', 'text/json')])
            effectJson = json.JSONEncoder().encode(effect.options)
            return [bytes(effectJson.encode())]

        elif (
            key.startswith('option-') and
            len(key.split('-')) >= 2 and
            key.split('-')[1] in effect.options.keys()
        ):
            option = key.split('-')[1]
            if value == '':
                start_response('200 OK', [('Content-Type', 'text/plain')])
                return [bytes(effect.options[option]['value'].encode())]
            elif validateOption(effect, option, value):
                effect.options[option]['value'] = value
                with open('/tmp/rgb-options.json', 'w') as file:
                    file.write(json.JSONEncoder().encode(effect.options))
                start_response('204 No Content', [
                               ('Content-Type', 'text/plain')])
                return [b'204 No Content']
            else:
                start_response('400 Bad Request', [
                               ('Content-Type', 'text/plain')])
                return [b'400 Bad Request']

        else:
            start_response('400 Bad Request', [('Content-Type', 'text/plain')])
            return [b'400 Bad Request']

    # If requesting any other path, return a file
    else:
        reqPath = os.path.join(SRC_ROOT, 'client', reqPath[1:])
        # If the file exists, return it
        if os.path.isfile(reqPath):
            start_response(
                '200 OK', [('Content-Type', mimetypes.guess_type(reqPath)[0])])
            with open(reqPath, 'rb') as file:
                return [file.read()]
        # If the file + '.html' exists, return it
        elif os.path.isfile(reqPath + '.html'):
            start_response('200 OK', [('Content-Type', 'text/html')])
            with open(reqPath + '.html', 'rb') as file:
                return [file.read()]
        # If the directory/index.html exists, return it
        elif os.path.isfile(os.path.join(reqPath, 'index.html')):
            start_response('200 OK', [('Content-Type', 'text/html')])
            with open(os.path.join(reqPath, 'index.html'), 'rb') as file:
                return [file.read()]
        # If the directory exists, list the contents
        elif os.path.isdir(reqPath):
            start_response('200 OK', [('Content-Type', 'text/plain')])
            return [bytes(f'Is directory: {repr(os.listdir(reqPath))}'.encode())]
        # If none of this stuff exists, return 404
        else:
            start_response('404 Not Found', [
                ('Content-Type', 'text/plain')])
            return [b'404 Not Found']


server = make_server('', PORT, serverApp)
server.serve_forever()
