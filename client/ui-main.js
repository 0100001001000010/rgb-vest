let effectChooser = document.getElementById('effect-chooser')
let colorSchemeChooser = document.getElementById('color-scheme-chooser')
let optionsLabel = document.getElementById('options-label')
let optionsContainer = document.getElementById('options-container')
let statusDisplay = document.getElementById('status-display')
let effectDescription = document.getElementById('effect-description')

function api(key, value) {
    if (value === undefined) {
        var query = key
    } else {
        var query = `${key}=${value}`
    }
    let xhr = new XMLHttpRequest()

    xhr.open('GET', `/api?${query}`)
    xhr.send()

    return new Promise((resolve, reject) => {
        xhr.onload = () => resolve(xhr.responseText)
        xhr.onerror = () => reject('error')
        xhr.ontimeout = () => reject('timeout')
    })
}

let colorSchemes = {}
let effects = {}
let options = {}
let effect

function populateChoices(element, choices) {
    for (choice in choices) {
        let uiChoice = document.createElement('option')
        uiChoice.value = choice
        uiChoice.innerText = choices[choice].name
        element.appendChild(uiChoice)
    }
}

api('colorSchemes').then(
    function (colorSchemesJson) {
        colorSchemes = JSON.parse(colorSchemesJson)
        populateChoices(colorSchemeChooser, colorSchemes)
        return api('colorScheme')
    },
    function (status) {
        statusDisplay.style.display = 'block'
        statusDisplay.innerText = 'Failed to load color schemes. Reload advised.'
    }
).then(
    function (currentColorScheme) {
        colorSchemeChooser.value = currentColorScheme
    },
    function (status) {
        statusDisplay.style.display = 'block'
        statusDisplay.innerText = 'Failed to query current color scheme. Reload advised.'
    }
)

function refreshEffectInfo() {
    effect = effectChooser.value
    api('effect', effect).then(
        function () {
            effectDescription.title = effects[effectChooser.value].description
            return api('options')
        },
        function (status) {
            statudDisplay.style.display = 'block'
            statusDisplay.innerText = 'Failed to change effect. Reload advised.'
        }
    ).then(
        function (optionsJson) {
            options = JSON.parse(optionsJson)
            while (optionsContainer.hasChildNodes()) {
                optionsContainer.removeChild(optionsContainer.firstChild)
            }

            if (Object.keys(options).length == 0) {
                optionsLabel.style.display = 'none'
            } else {
                optionsLabel.style.display = 'block'
                for (option in options) {
                    if (options[option].type in uiOptions) {
                        optionsContainer.appendChild(uiOptions[options[option].type](option))
                    }
                    else {
                        statusDisplay.style.display = 'block'
                        statusDisplay.innerText = 'Unsupported option received. Likely a bug somewhere.'
                    }
                }
            }
        },
        function (status) {
            statusDisplay.style.display = 'block'
            statusDisplay.innerText = 'Failed to query options. Reload advised.'
        }
    )
}

api('effects').then(
    function (effectsJson) {
        effects = JSON.parse(effectsJson)
        populateChoices(effectChooser, effects)
        effectDescription.title = effects[effectChooser.value].description
        return api('effect')
    },
    function (status) {
        statusDisplay.style.display = 'block'
        statusDisplay.innerText = 'Failed to load effects. Reload advised.'
    }
).then(
    function (currentEffect) {
        effectChooser.value = currentEffect
        refreshEffectInfo()
    },
    function (status) {
        statusDisplay.style.display = 'block'
        statusDisplay.innerText = 'Failed to query current effect. Reload advised.'
    }
)

colorSchemeChooser.addEventListener('change', () => {
    api('colorScheme', colorSchemeChooser.value).then(
        refreshEffectInfo,
        function (status) {
            statusDisplay.style.display = 'block'
            statusDisplay.innerText = 'Failed to change color scheme. Reload advised.'
        }
    )
})
effectChooser.addEventListener('change', refreshEffectInfo)