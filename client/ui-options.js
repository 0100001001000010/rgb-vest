let frequencyButtonTimeCaches = {}
let uiOptions = {
    frequency: function (optionId) {
        let rootElement = document.createElement('div')
        rootElement.id = optionId
        rootElement.class = 'frequency'

        let beatsPerTime = document.createElement('input')
        beatsPerTime.id = optionId + '-beats-per-time'
        beatsPerTime.class = 'frequency-beats-per-time'
        beatsPerTime.type = 'number'
        beatsPerTime.value = options[optionId].value
        beatsPerTime.addEventListener('change', event => {
            let parentId = event.target.parentNode.id
            let timePerBeat = document.getElementById(parentId + '-time-per-beat')
            let minuteCheckbox = document.getElementById(parentId + '-minutes')
            timePerBeat.value = 1 / event.target.value
            api('option-' + parentId, minuteCheckbox.checked ? event.target.value / 60 : event.target.value)
        })

        let timePerBeat = document.createElement('input')
        timePerBeat.id = optionId + '-time-per-beat'
        timePerBeat.class = 'frequency-time-per-beat'
        timePerBeat.type = 'number'
        timePerBeat.value = 1 / options[optionId].value
        timePerBeat.addEventListener('change', event => {
            let parentId = event.target.parentNode.id
            let beatsPerTime = document.getElementById(parentId + '-beats-per-time')
            let minuteCheckbox = document.getElementById(parentId + '-minutes')
            beatsPerTime.value = 1 / event.target.value
            api('option-' + parentId, minuteCheckbox.checked ? beatsPerTime.value * 60 : beatsPerTime.value)
        })

        let minuteCheckbox = document.createElement('input')
        minuteCheckbox.id = optionId + '-minutes'
        minuteCheckbox.class = 'frequency-minutes-checkbox'
        minuteCheckbox.type = 'checkbox'
        minuteCheckbox.checked = false
        minuteCheckbox.addEventListener('change', event => {
            let parentId = event.target.parentNode.id
            let beatsPerTime = document.getElementById(parentId + '-beats-per-time')
            let timePerBeat = document.getElementById(parentId + '-time-per-beat')
            if (event.target.checked) {
                beatsPerTime.value *= 60
                timePerBeat.value /= 60
            } else {
                beatsPerTime.value /= 60
                timePerBeat.value *= 60
            }
        })

        let button = document.createElement('input')
        button.id = optionId + '-button'
        button.class = 'frequency-button'
        button.type = 'button'
        button.value = 'Tap 5 times to set'
        frequencyButtonTimeCaches[rootElement.id] = []

        button.addEventListener('click', event => {
            let parentId = event.target.parentNode.id
            let beatsPerTime = document.getElementById(parentId + '-beats-per-time')
            let timePerBeat = document.getElementById(parentId + '-time-per-beat')
            let minuteCheckbox = document.getElementById(parentId + '-minutes')
            let timeCache = frequencyButtonTimeCaches[parentId]

            if (timeCache.length < 4) {
                timeCache.push(new Date().getTime())
            } else {
                timeCache.push(new Date().getTime())
                function averageDiffs(array) {
                    function average(array) {
                        function sum(array) {
                            result = 0
                            for (item of array) {
                                result += item
                            }
                            return result
                        }
                        return sum(array) / array.length
                    }
                    let diffs = []
                    for (let i = 0; i < array.length - 1; i++) {
                        diffs.push(array[i + 1] - array[i])
                    }
                    return average(diffs)
                }
                timePerBeat.value = minuteCheckbox.checked ? averageDiffs(timeCache) / 60000 : averageDiffs(timeCache) / 1000
                beatsPerTime.value = 1 / timePerBeat.value
                frequencyButtonTimeCaches[parentId] = [] //timeCache = [] doesn't work because it replaces the reference with a blank array, not the value.
                api('option-' + parentId, minuteCheckbox.checked ? beatsPerTime.value * 60 : beatsPerTime.value)
            }
        })

        rootElement.appendChild(document.createTextNode(options[optionId].name + ': frequency:'))
        rootElement.appendChild(beatsPerTime)
        rootElement.appendChild(document.createTextNode(', period:'))
        rootElement.appendChild(timePerBeat)
        rootElement.appendChild(document.createTextNode(', minutes?: '))
        rootElement.appendChild(minuteCheckbox)
        rootElement.appendChild(document.createTextNode(' '))
        rootElement.appendChild(button)
        return rootElement
    },
    color: function (optionId) {
        let rootElement = document.createElement('div')
        rootElement.id = optionId
        rootElement.appendChild(document.createTextNode(options[optionId].name + ': '))

        let colorPicker = document.createElement('input')
        colorPicker.id = optionId + '-input'
        colorPicker.class = 'color'
        colorPicker.type = 'color'
        colorPicker.value = options[optionId].value
        colorPicker.addEventListener('input', event => {
            api('option-' + event.target.parentNode.id, event.target.value.slice(1)).catch(
                function (status) {
                    statusDisplay.style.display = 'block'
                    statusDisplay.innerText = 'Failed to set color. Reload advised'
                }
            )
        })

        rootElement.appendChild(colorPicker)
        return rootElement
    },
    bool: function (optionId) {
        let rootElement = document.createElement('div')
        rootElement.id = optionId
        rootElement.appendChild(document.createTextNode(options[optionId].name + ': '))

        let checkbox = document.createElement('input')
        checkbox.id = optionId + '-input'
        checkbox.class = 'bool'
        checkbox.type = 'checkbox'
        checkbox.checked = options[optionId].value == 'true'
        checkbox.addEventListener('input', event => {
            api('option-' + event.target.parentNode.id, event.target.checked).catch(
                function (status) {
                    statusDisplay.style.display = 'block'
                    statusDisplay.innerText = 'Failed to set option. Reload advised'
                }
            )
        })

        rootElement.appendChild(checkbox)
        return rootElement
    },
    button: function (optionId) {
        let rootElement = document.createElement('div')
        rootElement.id = optionId
        rootElement.appendChild(document.createTextNode(options[optionId].name + ': '))

        let button = document.createElement('input')
        button.id = optionId + '-input'
        button.class = 'button'
        button.type = 'button'
        button.value = options[optionId].name
        button.addEventListener('click', event => {
            api('option-' + event.target.parentNode.id, 'true').catch(
                function (status) {
                    statusDisplay.style.display = 'block'
                    statusDisplay.innerText = 'Failed to send button press. Reload advised'
                }
            )
        })

        rootElement.appendChild(button)
        return rootElement
    },
    list: function (optionId) {
        let rootElement = document.createElement('div')
        rootElement.id = optionId
        rootElement.appendChild(document.createTextNode(options[optionId].name + ': '))

        let listOptionContainer = document.createElement('select')
        listOptionContainer.id = optionId + '-input'
        listOptionContainer.class = 'choice'
        for (choice of options[optionId].choices) {
            let listOption = document.createElement('option')
            listOption.value = choice
            listOption.innerText = choice
            listOptionContainer.appendChild(listOption)
        }

        listOptionContainer.value = options[optionId].value
        listOptionContainer.addEventListener('input', event => {
            api('option-' + event.target.parentNode.id, event.target.value).catch(
                function (status) {
                    statusDisplay.style.display = 'block'
                    statusDisplay.innerText = 'Failed to set color. Reload advised'
                }
            )
        })

        rootElement.appendChild(listOptionContainer)
        return rootElement
    },
    num: function (optionId) {
        let rootElement = document.createElement('div')
        rootElement.id = optionId
        rootElement.appendChild(document.createTextNode(options[optionId].name + ': '))

        let textField = document.createElement('input')
        textField.id = optionId + '-input'
        textField.class = 'num'
        textField.type = 'number'
        textField.min = options[optionId].min
        textField.max = options[optionId].max
        textField.value = options[optionId].value
        textField.addEventListener('input', event => {
            optionId = event.target.parentNode.id
            if (
                Number(options[optionId].min) <= Number(event.target.value) &&
                Number(options[optionId].max) >= Number(event.target.value)
            ) {
                api('option-' + optionId, event.target.value).catch(
                    function (status) {
                        statusDisplay.style.display = 'block'
                        statusDisplay.innerText = 'Failed to set value. Reload advised'
                    }
                )
            }
        })

        rootElement.appendChild(textField)
        return rootElement
    }
}